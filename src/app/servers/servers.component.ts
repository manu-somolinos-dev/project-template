import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {

  allowNewServer = false;

  serverCreationStatus = 'Any server has been created yet';

  serverName = '';

  servers = ['Server Alpha', 'Server Beta'];

  constructor() {
    setTimeout(() => {
      this.allowNewServer = true;
    }, 2000);
  }

  ngOnInit() {
  }

  getAllowNewServer() {
    return this.allowNewServer;
  }

  onCreateServer() {
    this.serverCreationStatus = 'A new server has been created';
    this.servers.push(this.serverName);
  }

  onUpdateServerName(event: Event) {
    this.serverName = (<HTMLInputElement>event.target).value;
  }

  onClearServer() {
    this.serverName = '';
  }
}
